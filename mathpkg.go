//
// go mod init gitlab.com/johndelavega/mathpkg -> go.mod is created
// go mod tidy -> go.mod is updated
//
package mathpkg

import (
	"fmt"

	"gitlab.com/johndelavega/addpkg"
)

const mVersion = "v0.1.0"

// Add two integer numbers
func Add(a int, b int) int {

	return addpkg.Add(a, b)
}

// PackageInfo returns info string
func PackageInfo() string {

	return fmt.Sprintf("mathpkg Version %s | gitlab.com/johndelavega/mathpkg", mVersion)

}

// Version returns version string for debugging
func Version() string {

	return fmt.Sprintf("%s", mVersion)

}
